/**
 * Classes
 */
class Shape {
    constructor(name, color) {
        this._name = name;
        this._color = color;
    }

    get name() { return this._name; }
    get color() { return this._color; }
}

class Circle extends Shape {
    constructor(name, color, radius) {
        super(name, color);
        this._radius = radius;
    }

    get radius() {
        return this._radius;
    }

    get area() {
        return Math.PI * Math.pow(this.radius, 2);
    }

    get definition() {
        return `${this.radius}`;
    }
}

class Rectangle extends Shape {
    constructor(name, color, width, height) {
        super(name, color);
        this._width = width;
        this._height = height;
    }

    get width() {
        return this._width;
    }

    get height() {
        return this._height;
    }

    get area() {
        return this.width * this.height;
    }

    get definition() {
        return `${this.width} x ${this.height}`;
    }
}

class Square extends Rectangle {
    constructor(name, color, size) {
        super(name, color, size, size);
    }

    get size() {
        return this.width;
    }
}

const printShape = shape => {
    const shapeClass = shape.constructor.name;
    const article = document.getElementById(shapeClass.toLowerCase());

    const paragraph = document.createElement('p');
    paragraph.style = `color: ${shape.color}`;
    paragraph.innerText = `The area of ${shapeClass} ${shape.name} (${shape.definition}) is ${shape.area}.`;

    article.append(paragraph);
};

/**
 * Circle stuff
 */
const circles = [
    new Circle('small', 'blue', 1),
    new Circle('medium', 'red', 5),
    new Circle('large', 'yellow', 10)
];

circles.forEach(circle => {
    printShape(circle);
});

/**
 * Rectangle stuff
 */
const rectangles = [
    new Rectangle('small', 'black', 2, 5),
    new Rectangle('medium', 'green', 5, 10),
    new Rectangle('large', 'orange', 100, 100)
];

rectangles.forEach(rectangle => {
    printShape(rectangle);
});

/**
 * Square stuff
 */
const squares = [
    new Square('small', 'pink', 100),
    new Square('medium', 'lime', 5),
    new Square('large', 'teal', 100)
];
squares.forEach(square => {
    printShape(square);
});
